# MDMA Policy MDMCDA

This project documents the analyses done for the MDMA Policy Think Tank that was active in the Netherlands in 2019-2020. The Think Tank used Multi Decision Multi Criteria Decision Analysis, which is an adaption of the MCDA procedure.

The main document is `mdma-policy-mdmcda.Rmd`, which loads the produced data and computes the results using the `mdmcda` R package (https://r-packages.gitlab.io/mdmcda). This document renders into `mdma-policy-mdmcda.html`, which is hosted by GitLab Pages at https://denktank.gitlab.io/mdma-policy-mdmcda.

The GitLab repository for this project is located at https://gitlab.com/denktank/mdma-policy-mdmcda, and the Open Science Framework repository for this project is located at https://osf.io/h58r6.
In addition, there is a website accompanying this project which is located at https://mdmabeleid.nl.
